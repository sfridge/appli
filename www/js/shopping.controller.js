angular.module('starter.controllers')

.controller('ShoppingCtrl', function($scope, Fridge, Increase, Decrease, ShoppingList){

	$scope.init = function(){
		var get = ShoppingList.get();
		get.$promise.then(function(data){
			$scope.fridge = data.response;
			console.log($scope.fridge);
		},function(error){
			console.log("error loading shopping list");
		});
	}

	$scope.doRefresh = function() {
		$scope.init();
    	$scope.$broadcast('scroll.refreshComplete');
  	}
	
	$scope.increase = function(beers){
		var inc = Increase.update({bar_code:beers.bar_code});
		$scope.error = false;
		inc.$promise.then(function(data){
		},function(error){
			$scope.error = true;
		});
		if($scope.error === false){
			beers.quantity += 1;
		}
	};
	
	$scope.decrease = function(beers){
		var dec = Decrease.update({bar_code:beers.bar_code});
		$scope.error = false;
		dec.$promise.then(function(data){
		},function(error){
			$scope.error = true;
			//error	
		});
		if($scope.error === false){
			beers.quantity -= 1;
		}
	};
	
})
