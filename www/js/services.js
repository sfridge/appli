var defaultURL =  "http://INSERT.YOUR.BACKEND.URL/api/" ;

angular.module('starter.services', ['ngResource'])
.factory('Fridge', function ($resource) {
    return $resource(defaultURL +"fridge");
})

.factory('ShoppingList', function ($resource) {
    return $resource(defaultURL +"shopping_list");
})

.factory('BarCode', function ($resource) {
    return $resource(defaultURL +"bar_code",{search:'@search'},{
		update:{
			method:'PUT'
		}		
	});
})

.factory('Increase', function ($resource) {
    return $resource(defaultURL +"bar_code/:bar_code/increase",{bar_code : '@bar_code', quantity : '@quantity'},{
		update:{
			method:'PUT'
		}
	});
})


.factory('Decrease', function ($resource) {
    return $resource(defaultURL +"bar_code/:bar_code/decrease",{bar_code : '@bar_code', quantity : '@quantity'},{
		update:{
			method:'PUT'
		}
	});
})


.factory('GetInfo', function ($resource) {
    return $resource("http://fr.openfoodfacts.org/api/v0/produit/:bar_code",{bar_code : '@bar_code'},{});
})


.factory('Exist', function ($resource) {
    return $resource(defaultURL +"bar_code/exist/:bar_code",{bar_code : '@bar_code'},{});
});



