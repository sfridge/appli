angular.module('starter.controllers')

.controller('ScannerCtrl', function($scope,$cordovaBarcodeScanner, Fridge, Exist, Increase, GetInfo, BarCode, toastr){
	
	$scope.fridge = {};
	$scope.fridge.quantity = 6;
	$scope.fridge.bar_code;
	$scope.fridge.name = "name";
	$scope.fridge.must_have = false;
	
	
	$scope.clickIcon = function(){
		console.log("test");
		document.addEventListener("deviceready", function () {
			$cordovaBarcodeScanner
		  	.scan()
		  	.then(function(barcodeData) {    	
				$scope.fridge.bar_code = barcodeData.text;
				// Success! Barcode data is here
		  	}, function(error) {
		    	$scope.error();
		  	});
		}, false);
	};
	
	
	$scope.addToFridge = function(){
		var found = Exist.get({bar_code:$scope.fridge.bar_code});
		found.$promise.then(function(data){
			
			if(data.response === true){
				var barCode = BarCode.get({search:$scope.fridge.bar_code});
					barCode.$promise.then(function(data){
						$scope.fridge.quantity += data.response.quantity
						$scope.fridge.name = data.response.name;
						var update = BarCode.update({search:$scope.fridge.bar_code, fridge:$scope.fridge});
						update.$promise.then(function(data){
							$scope.sucess();	
						},function(error){
							$scope.error();
						});
				},function(error){
					$scope.error();
				});
			}else{
				var bar_code = $scope.fridge.bar_code + ".json"
				var info = GetInfo.get({bar_code:bar_code});
				info.$promise.then(function(data){
					if(data.status_verbose ==="product found"){					
						var product = data.product;
						$scope.fridge.name = product.product_name +" - "+product.brands;
						$scope.fridge.volume = product.quantity;
						var post = Fridge.save({fridge:$scope.fridge});
						post.$promise.then(function(data){
							$scope.sucess();
						},function(error){
							$scope.error();
						});
					}else{
						$scope.notFound()
					}
				},function(error){
						$scope.error();
				});
			}
		})
		
	};

	$scope.sucess = function(){
		toastr.success('Sucessfully added');
	};

	$scope.error = function(){
		toastr.error('An error as occured');
	};
	
	$scope.notFound = function(){
		toastr.error('Product not found : check the code or add it on openfoodfact.org');
	};
})
