angular.module('starter.controllers')


.controller('StockCtrl', function($scope, Fridge, Increase, Decrease) {
	
	$scope.fridge = [];	


	$scope.init = function(){
		var get = Fridge.get();
		get.$promise.then(function(data){
			$scope.fridge = data.response;
			console.log($scope.fridge);
		},function(error){
			//error
		});
	}

	$scope.doRefresh = function() {
		$scope.init();
    	$scope.$broadcast('scroll.refreshComplete');
  	}
	
	$scope.increase = function(beers){
		var inc = Increase.update({bar_code:beers.bar_code});
		$scope.error = false;
		inc.$promise.then(function(data){
		},function(error){
			$scope.error = true;
		});
		if($scope.error === false){
			beers.quantity += 1;
		}
	};
	
	$scope.decrease = function(beers){
		var dec = Decrease.update({bar_code:beers.bar_code});
		$scope.error = false;
		dec.$promise.then(function(data){
		},function(error){
			$scope.error = true;
			//error	
		});
		if($scope.error === false){
			beers.quantity -= 1;
		}
	};
	
});

